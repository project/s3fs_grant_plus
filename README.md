# S3 File System Grant Plus

In certain scenarios, such as microservice applications, front-end users may
only require read access to files (such as images, videos, audio, etc.), while
file creation is managed by a separate entity like an administrator or a
Backoffice app. In such cases, for security reasons, it's imperative to
restrict front-end users to read-only permissions, preventing any modifications
or deletions of files. The S3 File System Grant Plus module addresses this
specific need effectively.


## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Features](#features)
- [Troubleshooting](#troubleshooting)
- [Maintainers](#maintainers)


## Requirements

- Drupal 9 or Drupal 10
- Drupal [S3FS](https://www.drupal.org/project/s3fs) module


## Installation

1. Download and place the module in your modules directory (`/modules/custom`).
2. Enable the module through the Drupal administration interface.
3. Ensure that the required S3FS module is also enabled.


## Configuration

1. Go to the s3fs configuration page (`/admin/config/media/s3fs`)
2. Configure your AWS credentials and S3 bucket settings.
3. Enter the account's ID that is supposed to have read-only access to files.


## Features

- Provides the ability to grant read-only permissions to another account within
  the S3 bucket, enhancing security and access control.


## Troubleshooting

When using this module, it's important to note that it prevents front-end users
from generating image styles on the fly. To address this issue, we recommend
automatically generating all image styles at the time of image creation.
For this purpose, we suggest utilizing the [Image Style Warmer](https://www.drupal.org/project/image_style_warmer) module, which
efficiently handles the automatic generation of image styles, ensuring smooth
functionality within your Drupal environment.


## Maintainers

- Mathieu Palouki - [mpalouki](https://www.drupal.org/u/mpalouki)
